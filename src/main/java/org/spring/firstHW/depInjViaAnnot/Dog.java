package org.spring.firstHW.depInjViaAnnot;

import org.springframework.stereotype.Component;

@Component
public class Dog implements Animal {
    @Override
    public void whatAnimalEat() {
        System.out.println("Dog eats bones");
    }
}
