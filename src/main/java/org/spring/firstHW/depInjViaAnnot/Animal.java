package org.spring.firstHW.depInjViaAnnot;

public interface Animal {
    void whatAnimalEat();
}
