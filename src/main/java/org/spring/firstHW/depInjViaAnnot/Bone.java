package org.spring.firstHW.depInjViaAnnot;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;

@Component
public class Bone {

    @Autowired
    @Qualifier("dog")
    Animal animal;

    public void dogEats() {
        animal.whatAnimalEat();
    }
}
