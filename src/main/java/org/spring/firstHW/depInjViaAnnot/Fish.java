package org.spring.firstHW.depInjViaAnnot;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;

@Component
public class Fish {

    @Autowired()
    @Qualifier("cat")
    Animal animal;

    public void catEats() {
        animal.whatAnimalEat();
    }

}
