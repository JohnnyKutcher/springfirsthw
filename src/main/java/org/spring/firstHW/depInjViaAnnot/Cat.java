package org.spring.firstHW.depInjViaAnnot;

import org.springframework.stereotype.Component;

@Component
public class Cat implements Animal {
    @Override
    public void whatAnimalEat() {
        System.out.println("Cat eats fishes");
    }
}
