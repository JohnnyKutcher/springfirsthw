package org.spring.firstHW.depInjViaXml;

public class University {

    private Student student;

    private Teacher teacher;

    public University(Teacher teacher) {
        this.teacher = teacher;
    }

    public Student getStudent() {
        return student;
    }

    public void setStudent(Student student) {
        this.student = student;
    }

    @Override
    public String toString() {
        return "University{" +
                "student=" + student +
                ", teacher=" + teacher +
                '}';
    }
}
