package org.spring.firstHW.configuration;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

@Configuration
@ComponentScan("org.spring.firstHW")
public class AppConfig {
}
