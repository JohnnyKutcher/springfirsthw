package org.spring.firstHW;

import org.spring.firstHW.configuration.AppConfig;
import org.spring.firstHW.depInjViaAnnot.Bone;
import org.spring.firstHW.depInjViaAnnot.Fish;
import org.spring.firstHW.depInjViaXml.University;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.context.support.AbstractApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

public class AppMain {
    public static void main(String[] args) {
        AbstractApplicationContext context = new ClassPathXmlApplicationContext("springHW-config.xml");
        University university = (University)context.getBean("university");
        System.out.println("University details: " + university);

        context.close();

        AbstractApplicationContext context1 = new AnnotationConfigApplicationContext(AppConfig.class);

        Fish fish = (Fish)context1.getBean("fish");
        fish.catEats();

        Bone bone = (Bone)context1.getBean("bone");
        bone.dogEats();

        context1.close();
    }
}
